module gitlab.com/commondatafactory/eanregister

go 1.16

require (
	github.com/lib/pq v1.10.3
	gorm.io/datatypes v1.2.0
	gorm.io/driver/postgres v1.5.7
	gorm.io/gorm v1.25.7
)
