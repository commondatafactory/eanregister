package main

import (
	"encoding/json"
	"fmt"
	"os"
	"testing"
	// "reflect"
)

func PrettyPrint(t *testing.T, v interface{}) (err error) {

	if !testing.Verbose() {
		return
	}
	b, err := json.MarshalIndent(v, "", "  ")
	if err == nil {
		t.Log(string(b))
	}
	return
}

func TestParseBody(t *testing.T) {
	f, err := os.Open("test/testresponse.json")

	if err != nil {
		t.Error("Expected to read test html page")
	}
	defer f.Close()

	result, err := parseBody(f)

	if err != nil {
		t.Error(err)
	}

	expected := &meteringPoints{
		MeteringPoints: []*meteringPoint{
			&meteringPoint{
				Ean: "871687140023349386",
				Address: address{
					PostalCode:   "1121RM",
					StreetNumber: 39,
					Street:       "Buitenhuislaan",
					City:         "LANDSMEER",
				},
				Product:              "GAS",
				Organisation:         "Liander N.V.",
				GridOperatorEan:      "8716871000002",
				GridArea:             "871718518003012572",
				SpecialMeteringPoint: false,
				BagId:                "0415010000005430",
			},
		},
		Message: "",
	}

	a, _ := json.MarshalIndent(expected, "", "   ")
	b, _ := json.MarshalIndent(result, "", "   ")

	if string(a) != string(b) {
		t.Error("Expected != result.")
		t.Errorf("%+v\n", string(a))
		t.Errorf("%+v\n", string(b))
	}
}

func TestMulti(t *testing.T) {

	f, err := os.Open("test/testmultiresponse.json")

	if err != nil {
		t.Error("Expected to read test json page")
	}
	defer f.Close()

	result, err := parseBody(f)
	if err != nil {
		t.Error(err)
	}

	expected := &meteringPoints{
		MeteringPoints: []*meteringPoint{
			&meteringPoint{
				Ean: "871689290102131246",
				Address: address{
					PostalCode:           "3021AD",
					StreetNumber:         61,
					StreetNumberAddition: "A",
					Street:               "Beukelsdijk",
					City:                 "ROTTERDAM",
				},
				Product:              "GAS",
				Organisation:         "Stedin Netbeheer B.V.",
				GridOperatorEan:      "8716892000005",
				GridArea:             "871718518003006014",
				SpecialMeteringPoint: true,
				BagId:                "",
			},
			&meteringPoint{
				Ean: "871689290102131239",
				Address: address{
					PostalCode:           "3021AD",
					StreetNumber:         61,
					StreetNumberAddition: "A",
					Street:               "Beukelsdijk",
					City:                 "ROTTERDAM",
				},
				Product:              "GAS",
				Organisation:         "Stedin Netbeheer B.V.",
				GridOperatorEan:      "8716892000005",
				GridArea:             "871718518003006014",
				SpecialMeteringPoint: true,
				BagId:                "",
			},
			&meteringPoint{
				Ean: "871689290102131253",
				Address: address{
					PostalCode:           "3021AD",
					StreetNumber:         61,
					StreetNumberAddition: "B",
					Street:               "Beukelsdijk",
					City:                 "ROTTERDAM",
				},
				Product:              "GAS",
				Organisation:         "Stedin Netbeheer B.V.",
				GridOperatorEan:      "8716892000005",
				GridArea:             "871718518003006014",
				SpecialMeteringPoint: false,
				BagId:                "0599010000091574",
			},
		},
		Message: "",
	}

	a, _ := json.MarshalIndent(expected, "", "   ")
	b, _ := json.MarshalIndent(result, "", "   ")

	if string(a) != string(b) {
		t.Error("Expected != result.")
		t.Errorf("%+v\n", string(a))
		t.Errorf("%+v\n", string(b))
	}

}

func TestNoResult(t *testing.T) {
	f, err := os.Open("test/noresult.json")

	if err != nil {
		t.Error("Expected to read test json page")
	}
	defer f.Close()

	result, err := parseBody(f)
	if err != nil {
		t.Error(err)
	}

	expected := &meteringPoints{
		MeteringPoints: []*meteringPoint{},
	}

	a, _ := json.MarshalIndent(expected, "", "   ")
	b, _ := json.MarshalIndent(result, "", "   ")

	if string(a) != string(b) {
		t.Error("Expected != result.")
		t.Errorf("%+v\n", string(a))
		t.Errorf("%+v\n", string(b))
	}
}

func TestSelect(t *testing.T) {

	// should be test database but we use
	// the 'actual' database for now
	db := connect()
	var count int

	SETTINGS.Set("gemeente", "Oostzaan", "testing municipality")
	fetchAddresses(db)

	expected := 4000
	count = len(chAddressSelect)
	if count < expected {
		t.Errorf("wrong count %d < %d", len(chAddressSelect), expected)
	}

	fmt.Println("count?", count)
}

// TestActualRequest we test a single adres with
// the actual service.
func TestActualRequest(t *testing.T) {

	a := addressSelect{
		Postcode:   "1121RM",
		Huisnummer: 39,
	}

	resp, err := doRequest(&a)

	if err != nil {
		t.Log(err)
		t.Error("request failed")
	}

	mp, err := parseBody(resp.Body)
	if err != nil {
		t.Error(err)
	}

	resp.Body.Close()

	if len(mp.MeteringPoints) != 1 {
		t.Errorf("we should have 1 result we got: %v", mp.MeteringPoints)
	}
}
