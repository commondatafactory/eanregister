# EANregister


scraping: https://gateway.edsn.nl/eancodeboek/

Extract EAN codes from EAN codeboek dot nl.
So we can 'exactly' determine addresses with a
gas and or electric connection

Using addresses from nlextract
we ask the EAN codeboek API for EAN connection information

This code uses BAG of the Netherlands and a custom created
table with all current active adresses in NL.

# How the project works:

Requitements `baghelp.all_pvsl_nums` database table containing
CBS 2023 Administrative boundaries.


## Step 1 Fetch data

 - Fetch postcode and number which we have not queried yet
 - Query API eancodebook
 - Fetch meteringPoints
 - Store Meterpoints JSON repsonses

## Step 2 Fetch matching adresses

 - TODO
 - Look for best matching Adress
 - Store provided information

## Run the code

        0) register for an API key.

	1) set the EANAPIKEY environment variable.

        2) BAG address table is needed
Test

    go test

Build:

    go build

	2) ./eanregister

    To only pick a municipality

        2.1) ./eanregiter -gemeente Landsmeer

Now you should download about 100 req / s ean registration information. We store empty
records in case there is no EAN code this way we keep track of tried addresses.

For more options

	eanregister -h


