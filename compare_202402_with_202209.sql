select count(*) from eancodeboek.eancodes202402responses er;

select sum('count') from eancodeboek.eancodes202402responses er;

select sum(count)  from eancodeboek.eancodes202402responses er;

select count(*) from eancodeboek.eancodes202402responses er where count = 0;
select count(*) from eancodeboek.eancodes202402responses er;

select count(*) from eancodeboek.eancodes202402responses er where count = 100;


select sum(count) from ean.numid_ean_counts_202209 nec;


select * from baghelp.all_pvsl_nums apn ;
select * from eancodeboek.eancodes202402responses er;
/* Extract metering points */

drop table if exists eancodeboek.meteringPoints;
/* we extract individual metering points address and bag information from eancodes to match */
select
        id,
        mp->>'BagId' as bagid,
        mp->'Address'->>'PostalCode' as postcode,
        mp->'Address'->>'StreetNumber' as huisnummer,
        mp->'Address'->>'StreetNumberAddition' as toevoeging,
        mp->>'Ean' as eancode,
        mp->>'GridArea' as gridarea,
        mp->>'GridOperatorEan' as gridoperatorean,
        mp->>'Organisation' as organisation
into eancodeboek.meteringPoints
from eancodeboek.eancodes202402responses er
cross join jsonb_array_elements(response->'MeteringPoints') as mp    --left outer join baghelp.all_pvsl_nums apn on apn.vid = response->'MeteringPoints'->>'BagId' where numid is not null limit 10

/* check should be equal */
select count(*) from eancodeboek.meteringPoints;
select count(distinct(eancode)) from eancodeboek.meteringPoints;

/* create a dataset per postcode */
select postcode, gridarea, gridoperatorean, organisation, count(*) 
into eancodeboek.postcode_gridarea
from eancodeboek.meteringpoints
group by postcode, gridarea, gridoperatorean, organisation;

create index on eancodeboek.postcode_gridarea (postcode);
--create index on eancodeboek.meteringpoints_v2 ()

select postcode, count(*) from eancodeboek.postcode_gridarea group by postcode

select * from eancodeboek.postcode_gridarea where postcode = '7461PD';

/* research gridArea */
select
        n.numid,
        n.vid,
        n.geopunt,
        mp.id,
        mp.postcode,
        mp.huisnummer,
        mp.toevoeging,
        mp.eancode,
        mp.gridarea,
        mp.organisation,
        mp.gridoperatorean
into eancodeboek.meteringPoints_nums_geo
from eancodeboek.meteringPoints mp
left outer join baghelp.all_pvsl_nums n on n.vid = mp.bagid
where n.vid is not null;

create index on eancodeboek.meteringPoints_nums_geo using gist(geopunt);


/* find all matching numid matching with vid */
drop table if exists eancodeboek.meteringPoints_nums;
select
        n.numid,
        n.vid,
        n.geopunt,
        mp.id,
        mp.postcode,
        mp.huisnummer,
        mp.toevoeging,
        mp.eancode,
        mp.gridarea,
        mp.organisation,
        mp.gridoperatorean
into eancodeboek.meteringPoints_nums
from eancodeboek.meteringPoints mp
left outer join baghelp.all_pvsl_nums n on n.vid = mp.bagid
where n.vid is not null;
--select * from ean.

select * from eancodeboek.meteringPoints_nums;
create index on eancodeboek.MeteringPoints (bagid);
create index on eancodeboek.meteringPoints_nums (vid);
create index on eancodeboek.meteringPoints_nums (numid);


drop table if exists eancodeboek.meteringPoints_nums_lh;
/* now we find all metering points for which we did not find a bagid.
 * by doing a levenshtein match on huisnummer - toevoeging
 *
 * */
select
        n.numid,
        n.vid,
        n.geopunt,
        mp.id,
        mp.postcode,
        mp.huisnummer,
        mp.toevoeging,
        mp.eancode,
        mp.gridarea,
        mp.organisation,
        mp.gridoperatorean
        --,
        --n.numid,
        --levenshtein(
        --      concat(mp.huisnummer, mp.toevoeging),
        --      concat(n.huisnummer, n.huisletter, n.huisnummertoevoeging)
        --) match_score,
        --n.huisnummer,
        --n.huisletter,
        --n.huisnummertoevoeging
into eancodeboek.meteringPoints_nums_lh
from (
        select mp.id, bagid, mp.postcode, mp.huisnummer, mp.toevoeging, mp.eancode, mp.gridarea, mp.organisation, mp.gridoperatorean
        from eancodeboek.meteringPoints mp
        left outer join eancodeboek.meteringPoints_nums mpn on (mpn.vid = mp.bagid)
        where mpn.vid is null
) mp,
lateral (
        select
                n.numid,
                n.vid,
                n.geopunt,
                n.huisnummer,
                n.huisletter,
                n.huisnummertoevoeging
        from baghelp.all_pvsl_nums n
        where mp.postcode = n.postcode and mp.huisnummer::numeric = n.huisnummer
        order by levenshtein(
                concat(n.huisnummer, n.huisletter, n.huisnummertoevoeging),
                concat(mp.huisnummer, mp.toevoeging)
        ) limit 1
) n

/*
 * Create complete table of eancodes coupled to BAG.
 */

drop table if exists eancodeboek.metering_points_nums;
select
        *
into eancodeboek.metering_points_nums
from (
select * from eancodeboek.meteringPoints_nums_lh
union
select * from eancodeboek.meteringPoints_nums
) e

create index on eancodeboek.metering_points_nums (numid);
CREATE INDEX ON eancodeboek.metering_points_nums USING GIST (geopunt);


select * from eancodeboek.metering_points_nums;


select count(*) from eancodeboek.metering_points_nums;
select count(distinct(eancode)) from eancodeboek.metering_points_nums;

DROP TABLE IF EXISTS eancodeboek.gridarea_geo;
select organisation, gridarea, st_multi(
				 st_makeValid(
				 st_union(
				 	st_convexhull(
				 		st_buffer(geopunt, 250, 'quad_segs=3')
				 	)
				 ))) as geocollection,
				 count(*) AS gasconnections
INTO eancodeboek.gridarea_geo
from eancodeboek.metering_points_nums group by gridarea, organisation;

SELECT * FROM eancodeboek.gridarea_geo;
CREATE INDEX ON eancodeboek.gridarea_geo 


drop table if exists eancodeboek.verdachte_punten;


SELECT mn.* INTO eancodeboek.verdachte_punten FROM eancodeboek.meteringpoints_nums mn, eancodeboek.gridarea_geo gg WHERE st_contains(gg.geocollection, mn.geopunt) AND gg.gridarea != mn.gridarea;



drop table if exists eancodeboek.difference;

select
        mp.eancode,
        "count" count_202209,
        mp.postcode,
        mp.huisnummer,
        mp.geopunt,
        case
                when mp.numid is not null then mp.numid
                when nec.numid is not null and mp.numid is null then nec.numid
        end as numid,
        case
                when mp.numid is null then 1  -- missing
                when nec.numid is null then 2 -- new
                when nec.numid is not null and mp.numid is not null then 0 -- existing
        end as status
into eancodeboek.difference
from eancodeboek.metering_points_nums mp
full outer join ean.numid_ean_counts_202209 nec on nec.numid = mp.numid

-- where mp.numid is null;

select count(*) from eancodeboek.difference where status = 2; -- new
select count(*) from eancodeboek.difference where status = 1; -- missing
select count(*) from eancodeboek.difference where status = 0; -- no change

select count(*) from eancodeboek.difference where numid is null;

drop table if exists eancodeboek.difference_geo;

select
        eancode,
        count_202209,
        status,
        n.postcode,
        n.huisnummer,
        n.geopunt,
        d.numid
into eancodeboek.difference_geo
from eancodeboek.difference d
left outer join baghelp.all_pvsl_nums n on (n.numid=d.numid)
--where eancode = '871687140013028635'

create index on eancodeboek.difference_geo using GIST (geopunt);
create index on eancodeboek.difference_geo (numid);
