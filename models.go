package main

import (
	"encoding/json"

	"gorm.io/datatypes"
	"gorm.io/gorm"
)

// collect EAN data which comes from the eancodeboek.nl
type EANcode struct {
	gorm.Model
	Numid                string `gorm:"INDEX"`
	Pid                  string
	Vid                  string
	Lid                  string
	Sid                  string
	Straatnaam           string
	Postcode             string `gorm:"INDEX"`
	Huisnummer           uint64
	Huisnummertoevoeging string
	Huisletter           string
	Woonplaatsnaam       string
	MatchScore           uint64
	Toevoeging           string
	Product              string
	// Nummeraanduiding string `gorm:"INDEX"`
	EANcode string `gorm:"INDEX"`
}

func (EANcode) TableName() string {
	return "eancodeboek.eancodes202402"
}

type MeteringPointsResponse struct {
	gorm.Model
	Postcode   string `gorm:"INDEX"`
	Huisnummer uint64
	Product    string
	Count      int
	Response   datatypes.JSON
}

// create new MeteringPoints instance using adress and API response
// information
func CreateMeteringPointsResponse(a *addressSelect, mp *meteringPoints) (MeteringPointsResponse, error) {
	newMP := MeteringPointsResponse{}
	newMP.Product = SETTINGS.Get("segment")

	jsonr, err := json.Marshal(mp)
	if err != nil {
		return newMP, err
	}
	newMP.Postcode = a.Postcode
	newMP.Huisnummer = a.Huisnummer
	newMP.Response = jsonr
	newMP.Count = len(mp.MeteringPoints)
	return newMP, nil
}

func (MeteringPointsResponse) TableName() string {
	return "eancodeboek.eancodes202402responses"
}
