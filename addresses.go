package main

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"io"

	"gorm.io/driver/postgres"
	"gorm.io/gorm"

	//"io/ioutil"
	"log"
	"net/http"
	"net/http/httputil"
	"strconv"
	"sync"
	"testing"
	"time"
)

// globals
var wg sync.WaitGroup
var wgStore sync.WaitGroup

// var wgFetch sync.WaitGroup
var EMPTY int
var ROWCOUNT int
var processedCount int
var codebookurl string

var limiter chan *time.Time
var chMeteringPoints chan *MeteringPointsResponse

var track_postcode string

type addressSelect struct {
	Postcode   string
	Huisnummer uint64
	// Woonplaatsnaam   string
}

var chAddressSelect chan *addressSelect

func init() {
	// make sure testing flags are present
	testing.Init()
	// run settings
	SETTINGS.SetInt("workers", 7, "Specify mount of http client tasks")
	SETTINGS.SetInt("requests", 100, "max requests a second done")

	//
	SETTINGS.Set("EANAPIKEY", "MISSING", "set your key from https://gateway.edsn.nl/eancodeboek/")

	// postgres settings
	SETTINGS.Set("dbhost", "127.0.0.1", "Specify postgres db hostname")
	SETTINGS.Set("dbpwsd", "insecure", "Set Database Password")
	SETTINGS.Set("dbname", "cdf", "Set database name")
	SETTINGS.Set("dbuser", "cdf", "Set database user")
	SETTINGS.SetInt("dbport", 5433, "Specify database port")

	SETTINGS.Set("gemeente", "NL", "Specify gemeente to collect codes for (EDIT SQL) ")
	SETTINGS.Set("segment", "Gas", "Specify Gas or Elk")
	SETTINGS.SetBool("meteringpoints", true, "store meteringpoints only")

	SETTINGS.Parse()

	processedCount = 0

	codebookurl = "https://gateway.edsn.nl/eancodeboek/v1/ecbinfoset"

	limiter = make(chan *time.Time)
	chAddressSelect = make(chan *addressSelect, 10000)
	chMeteringPoints = make(chan *MeteringPointsResponse, 1000)

	go speedlimiter()

}

func speedlimiter() {
	rs := SETTINGS.GetInt("requests")
	delta := time.Duration(1000 / rs)
	for t := range time.Tick(delta * time.Millisecond) {
		limiter <- &t
	}
}

/*
Using Prepared adres table with unique postcode huisnumbers.

baghelp.all_pvsl_nums contains all standplaats / ligplaatsen / verblijfsobjecten
*/
func fetchGemeenteSelect(db *gorm.DB) *gorm.DB {
	query := `select gemeentecode, gemeentenaam from cbs2023.gemeenten`
	gemeente := SETTINGS.Get("gemeente")
	if len(gemeente) > 2 {
		query = fmt.Sprintf(`
	    select
		gemeentecode,
		gemeentenaam
	    from cbs2023.gemeenten
	    where gemeentenaam = '%s'
	    `, gemeente)
	}

	r := db.Raw(query)
	return r
}

// AddressSelect find addresses for which we did not
// yet searched for EAN codes yet
// We work by municipality chunks
// return unique Postcode - Huisnummer
func fetchAddresses(db *gorm.DB) {
	var gemeentenaam sql.NullString
	var gemeentecode sql.NullString

	// Fetch gemeente list
	gemrows, err := fetchGemeenteSelect(db).Rows()
	if err != nil {
		log.Fatal(err)
	}
	defer gemrows.Close()

	// For each gemeente we check which
	// addresses we still have to check.
	for gemrows.Next() {
		err := gemrows.Scan(&gemeentecode, &gemeentenaam)

		if err != nil {
			log.Fatal(err)
		}

		gcode := convertSqlNullString(gemeentecode)
		gnaam := convertSqlNullString(gemeentenaam)
		log.Printf("Working on %s %s\n", gcode, gnaam)

		// now find missing postalcode , numbers to work on
		findMissingScrapeActions(db, gcode)
	}

	// all addresses are added.
	close(chAddressSelect)
	log.Println("done loading addresses:", ROWCOUNT)
}

// find all postalcodes - housenumbers within a municipality
// that have not been requested yet.
func extractPostalcodes(db *gorm.DB, gemeentecode string) *gorm.DB {
	// We need the name of response table
	r := MeteringPointsResponse{}
	responseTable := r.TableName()

	product := SETTINGS.Get("segment")

	// find postalcodes and housnumbers we have not queried yet.
	query := fmt.Sprintf(`
		select
		    n1.postcode,
		    n1.huisnummer
		FROM baghelp.all_pvsl_nums n1
		LEFT OUTER JOIN %s r on (
		    n1.postcode=r.postcode and
		    n1.huisnummer=r.huisnummer and
		    '%s'=r.product
		)
		LEFT OUTER JOIN baghelp.num_buurt_wijk_gemeente_provincie  m on (m.numid = n1.numid)
		WHERE r.id is null
		AND (m.gemeentecode = '%s' )
		group by n1.postcode, n1.huisnummer
		order by n1.postcode
		`, responseTable, product, gemeentecode)

	rows := db.Raw(query)
	return rows
}

func findMissingScrapeActions(db *gorm.DB, gemeentecode string) {

	rows, err := extractPostalcodes(db, gemeentecode).Rows()

	if err != nil {
		log.Fatal(err)
	}

	defer rows.Close()

	var huisnummer sql.NullInt64
	var postcode sql.NullString

	municipalityCount := 0
	for rows.Next() {
		ROWCOUNT++
		municipalityCount += 1

		err := rows.Scan(
			&postcode,
			&huisnummer,
		)
		if err != nil {
			log.Fatal(err)
		}
		a := addressSelect{
			Postcode:   convertSqlNullString(postcode),
			Huisnummer: convertSqlNullInt(huisnummer),
		}
		chAddressSelect <- &a
	}
	fmt.Printf("Done with %s %d", gemeentecode, municipalityCount)
}

func connect() *gorm.DB {
	db, err := gorm.Open(postgres.Open(ConnectStr()), &gorm.Config{})
	// db.LogMode(true)

	if err != nil {
		log.Println("db-error")
		log.Println(err)
	}

	return db
}

func closeDB(db *gorm.DB) {
	dbInstance, _ := db.DB()
	_ = dbInstance.Close()
}

func main() {

	db := connect()

	defer closeDB(db)

	log.Printf("connection succesfull")

	workers := SETTINGS.GetInt("workers")
	// Migrate the schema
	db.AutoMigrate(&EANcode{})
	db.AutoMigrate(&MeteringPointsResponse{})
	log.Println("automigrated tables")

	// fetch addresses
	go fetchAddresses(db)
	// start storing into DB action
	go storeMeteringPoint(db)

	wg.Add(workers)

	for i := 0; i < workers; i++ {
		go workerFetchEAN(i, chAddressSelect, chMeteringPoints)
	}

	go printStatus(chAddressSelect, chMeteringPoints)

	wg.Wait()

	//check fix channel closing.

	// close(chEANcodes) // wait until all requests are stored
	close(chMeteringPoints)
	wgStore.Wait()
	log.Printf("done. rowCount %d", ROWCOUNT)
}

func printStatus(chItems chan *addressSelect, chDB chan *MeteringPointsResponse) {
	i := 1
	delta := 5
	duration := 0
	speed := 0
	lastRowCount := -1

	for {
		time.Sleep(time.Duration(delta) * time.Second)

		if processedCount == lastRowCount {
			log.Println("loading halted..", processedCount)
			if processedCount == 0 {
				i = 1
			}
		}

		log.Printf("STATUS: rows:%-10d  %-10d rows/sec %s empty: %d buffers: %d DB: %d",
			processedCount, speed, track_postcode,
			EMPTY,
			len(chItems), len(chDB))
		duration = i * delta
		speed = processedCount / duration
		i++
		lastRowCount = processedCount
	}
}

// Store parsed EAN code results in database
// matched with BAG record
func storeMeteringPoint(db *gorm.DB) {
	wgStore.Add(1)
	for m := range chMeteringPoints {
		// store eancode in database
		db.Create(m)
		processedCount += 1
	}
	wgStore.Done()
}

// var bestMatch string = `
//     select
// 	    n.numid, pid, vid, lid, sid,
// 	    openbareruimtenaam,
// 	    m.gemeentenaam,
// 	    huisletter,
// 	    huisnummertoevoeging,
// 	    levenshtein(concat(n.huisnummer, ' ', lower(n.huisletter) , lower(n.huisnummertoevoeging)),
// 			concat(?::numeric, ' ', lower(?::text) )) as match_score
//     from baghelp.all_pvsl_nums n
//     left outer join baghelp.num_buurt_wijk_gemeente_provincie m on (m.numid = n.numid)
//     where
// 	    postcode = ?::text
//     AND     huisnummer = ?::numeric
//     ORDER BY levenshtein(concat(n.huisnummer, ' ', lower(n.huisletter) , lower(n.huisnummertoevoeging)),
// 			 concat(?::numeric, ' ', lower(?::text)) )
//     limit 1
// `

/*
func storeEAN(db *gorm.DB) {

	var numid, pid, vid, lid, sid, huisletter sql.NullString
	var openbareruimtenaam sql.NullString
	var woonplaatsnaam sql.NullString
	var huisnummertoevoeging sql.NullString
	var matchScore sql.NullInt64

	wgStore.Add(1)

	for mp := range chMeteringPoints {

		// using prepared stmt.
		nr := fmt.Sprintf("%d", e.Huisnummer)
		rows, err := db.Raw(
			bestMatch,
			nr, e.Toevoeging,
			e.Postcode, // e.Numid,
			nr, nr, e.Toevoeging).Rows()

		if err != nil {
			log.Fatal(err)
		}

		rowCount := 0
		// should only be 1 row
		for rows.Next() {
			if rowCount != 0 {
				log.Fatal("to many BAG records for EAN code")
			}

			if matchScore.Int64 > 2 {
				fmt.Printf("%+v", e)
				fmt.Println("Huisnummer", e.Huisnummer)
				fmt.Println(bestMatch)
			}

			err := rows.Scan(
				&numid, &pid, &vid, &lid, &sid,
				&openbareruimtenaam,
				&woonplaatsnaam,
				&huisletter,
				&huisnummertoevoeging,
				&matchScore,
			)

			if err != nil {
				log.Fatal(err)
			}

			// update EAN code with BAG data
			e.Numid = convertSqlNullString(numid)
			e.Pid = convertSqlNullString(pid)
			e.Vid = convertSqlNullString(vid)
			e.Lid = convertSqlNullString(lid)
			e.Sid = convertSqlNullString(sid)
			e.Straatnaam = convertSqlNullString(openbareruimtenaam)
			e.MatchScore = convertSqlNullInt(matchScore)

			// store eancode in database
			db.Create(e)

			track_postcode = e.Postcode
			processedCount += 1
			rowCount += 1
		}
		rows.Close()
	}
	wgStore.Done()
}
*/

func doRequest(a *addressSelect) (*http.Response, error) {

	markt := SETTINGS.Get("Product")

	segment := "GAS"
	if markt == "ELK" {
		segment = "ELK"
	}

	hn := strconv.FormatUint(a.Huisnummer, 10)

	req, _ := http.NewRequest(http.MethodGet, codebookurl, nil)

	q := req.URL.Query()

	q.Add("product", segment)
	q.Add("streetNumber", hn)
	q.Add("postalCode", a.Postcode)
	q.Add("limit", "100")

	req.URL.RawQuery = q.Encode()
	// fmt.Println(req.URL.RawQuery)
	req.Header.Add("Content-Type", "application/json")

	if SETTINGS.Get("EANAPIKEY") == "MISSING" {
		log.Fatal("EANAPIKEY API KEY MISSING/NOT SET")
	}

	req.Header.Add("X-API-Key", SETTINGS.Get("EANAPIKEY"))

	client := &http.Client{}
	resp, err := client.Do(req)

	//debug output

	/*
		// breaks parsing!!
		bytes, _ := ioutil.ReadAll(resp.Body)
		log.Print(req.URL.RawQuery)
		log.Print("woops parsing will break now.")
		log.Print(string(bytes))
		log.Print(resp.StatusCode)
		log.Print(resp.StatusCode)
	*/

	if err != nil {
		log.Fatal(err)
		return nil, err
	}

	return resp, nil

}

// fetch an adress and do an request.
func workerFetchEAN(i int, chAddressSelect chan *addressSelect, chMP chan *MeteringPointsResponse) {

	defer wg.Done()

	for a := range chAddressSelect {
		//limit the amount of requests we do.
		<-limiter

		resp, err := doRequest(a)
		if err != nil {
			log.Print("skipping")
			continue
		}

		//respDump, err := httputil.DumpResponse(resp, true)
		//if err != nil {
		//	log.Fatal(err)
		//}
		//fmt.Printf("RESPONSE:\n%s", string(respDump))

		mp, err := parseBody(resp.Body)

		if err != nil {
			log.Println(err)
			log.Printf("%+v", a)
			respDump, err := httputil.DumpResponse(resp, true)
			if err != nil {
				log.Fatal(err)
			}

			log.Printf("RESPONSE:\n%s", string(respDump))
			log.Fatal("stopped on error")
		}

		if mp.Message != "" {
			log.Printf("%v %+v\n", a, mp.Message)
		}
		resp.Body.Close()

		if len(mp.MeteringPoints) == 0 {
			EMPTY++
		}
		mpDB, err := CreateMeteringPointsResponse(a, mp)
		if err != nil {
			log.Fatal(err)
		}
		chMP <- &mpDB
	}

}

type address struct {
	PostalCode           string
	StreetNumber         int64
	StreetNumberAddition string
	Street               string
	City                 string
}

type meteringPoint struct {
	Ean                  string
	Address              address
	Product              string
	Organisation         string
	GridOperatorEan      string
	GridArea             string
	SpecialMeteringPoint bool
	BagId                string
}

type meteringPoints struct {
	MeteringPoints []*meteringPoint
	Message        string
}

// extract Resultable data from html document
func parseBody(body io.Reader) (r *meteringPoints, err error) {

	dec := json.NewDecoder(body)
	var mp meteringPoints
	dec.DisallowUnknownFields()

	err = dec.Decode(&mp)

	if err != nil {
		log.Println(err)
	}

	return &mp, err
}
