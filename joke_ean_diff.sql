select
        --mp.eancode,
        new_mp.numid,
        new_mp.pid,
        "count" count_202209,
        new_mp.postcode,
        new_mp.huisnummer,
        new_mp.geopunt,
        case
                when new_mp.numid is not null then new_mp.numid
                when old_nec.numid is not null and new_mp.numid is null then old_nec.numid
        end as numid,
        case
                when new_mp.numid is null then 1  -- missing
                when old_nec.numid is null then 2 -- new
                when old_nec.numid is not null and new_mp.numid is not null then 0 -- existing
        end as status
-- into eancodeboek.difference
from eancodeboek.metering_points_nums new_mp
full outer join ean.numid_ean_counts_202209 old_nec on old_nec.numid = new_mp.numid

-- where mp.numid is null;

select count(*) from eancodeboek.difference where status = 2; -- new
select count(*) from eancodeboek.difference where status = 1; -- missing
select count(*) from eancodeboek.difference where status = 0; -- no change

select count(*) from eancodeboek.difference where numid is null;

drop table if exists eancodeboek.difference_geo;

drop table if exists eancodeboek.difference_geo_joke;
select
        -- eancode,
        count_202209,
        status,
        n.postcode,
        n.huisnummer,
        n.geopunt,
        d.numid,
        nbwgp.buurtcode,
        nbwgp.gemeentecode,
        nbwgp.gemeentenaam,
        nbwgp.naam
into eancodeboek.difference_geo_joke
from eancodeboek.difference d
left outer join baghelp.all_pvsl_nums n on (n.numid=d.numid)
left outer join baghelp.num_buurt_wijk_gemeente_provincie nbwgp on (nbwgp.numid=d.numid);