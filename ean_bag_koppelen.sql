select count(*) from eancodes202108;

/* total amount of scraped from eancodeboek */
select * from public.eancodes202108 e;


/* create normalized set of unique ean codes */
drop table if exists ean.found_ean_codes;
select distinct postcode, huisnummer, lower(toevoeging) toevoeging, ea_ncode
into ean.found_ean_codes from public.eancodes202108 e where ea_ncode != '';

select count(*) from ean.found_ean_codes fec;
select count(distinct ea_ncode) from ean.found_ean_codes fec;

/* try to exact match on huisletter with bag */
drop table if exists ean.match_huisletter;
select
	pvn.pid,
	pvn.numid,
	pvn.postcode,
	pvn.huisnummer,
	pvn.huisletter,
	pvn.huisnummertoevoeging,
	-- e.postcode, e.huisnummer, e.toevoeging,
	e.ea_ncode
into ean.match_huisletter
from ean.found_ean_codes e left outer join baghelp.pand_vbo_nums pvn on (pvn.postcode = e.postcode and pvn.huisnummer = e.huisnummer and lower(pvn.huisletter) = e.toevoeging)
where pvn.pid is not null and e.toevoeging != '';

create index on ean.match_huisletter (ea_ncode);

/* try to exact match on huisnummertoevoeging
 * and not matched before
 * */
drop table if exists ean.match_toevoeging;
select
	pvn.pid,
	pvn.numid,
	pvn.postcode,
	pvn.huisnummer,
	pvn.huisletter,
	pvn.huisnummertoevoeging,
	-- e.postcode, e.huisnummer, e.toevoeging,
	e.ea_ncode
into ean.match_toevoeging
from ean.found_ean_codes e
left outer join baghelp.pand_vbo_nums pvn on (pvn.postcode = e.postcode and pvn.huisnummer = e.huisnummer and lower(pvn.huisnummertoevoeging) = e.toevoeging)
left outer join ean.match_huisletter mh on (mh.ea_ncode = e.ea_ncode)
where pvn.pid is not null and e.toevoeging != ''
and mh.ea_ncode is null;

create index on ean.match_toevoeging (ea_ncode);

/* try to exact match on empty toevoeging
 * and not matched before
 * */
drop table if exists ean.match_empty_toevoeging;
select
	pvn.pid,
	pvn.numid,
	pvn.postcode,
	pvn.huisnummer,
	pvn.huisletter,
	pvn.huisnummertoevoeging,
	-- e.postcode, e.huisnummer, e.toevoeging,
	e.ea_ncode
into ean.match_empty_toevoeging
from ean.found_ean_codes e
left outer join baghelp.pand_vbo_nums pvn on (pvn.postcode = e.postcode and pvn.huisnummer = e.huisnummer)
where pvn.pid is not null and e.toevoeging = '' and pvn.huisnummertoevoeging is null and pvn.huisletter is null;

select count(*) from ean.match_empty_toevoeging met;
select count(*) from ean.match_huisletter mh;
select count(*) from ean.match_toevoeging mt;

select count(*) from ean.found_ean_codes fec;

/* create a table of all direct matched ean codes */
drop table if exists ean.matched_direct;
select *
into ean.matched_direct
from (
select * from ean.match_huisletter mh
union
select * from ean.match_toevoeging mt
union
select * from ean.match_empty_toevoeging
) matched

create index on ean.matched_direct (ea_ncode);

/* find all ean codes that do not have a direct match yet */
drop table if exists ean.not_yet_matched;
select fec.postcode, fec.huisnummer, fec.toevoeging, fec.ea_ncode
into ean.not_yet_matched
from ean.found_ean_codes fec
left outer join ean.matched_direct md on md.ea_ncode = fec.ea_ncode
where md.pid is null

create index on ean.not_yet_matched (postcode);
create index on ean.not_yet_matched (huisnummer);

select count(*) from ean.not_yet_matched nym;

CREATE EXTENSION fuzzystrmatch;
create index on baghelp.pand_vbo_nums (postcode);
--create index on baghelp.pand_vbo_nums ()

drop table if exists ean.ean_levenshtein_matched;
/* match the left over (150.000) with best possible match */
select
	p.pid,
	p.numid,
	p.postcode,
	p.huisnummer,
	p.huisletter,
	p.huisnummertoevoeging,
	p.ea_ncode,
	--toevoeging_score
into ean.ean_levenshtein_matched
from (
	select b.huisletter, b.huisnummertoevoeging, e.*, b.pid, b.numid
	       , levenshtein(concat(lower(huisletter) , lower(huisnummertoevoeging)), lower(toevoeging)) toevoeging_score
	from ean.not_yet_matched e,
	lateral ( select pa.pid, pa.numid, pa.huisletter, pa.huisnummertoevoeging
	          from baghelp.pand_vbo_nums pa
	          where pa.postcode = e.postcode
	          and pa.huisnummer = e.huisnummer
	          order by levenshtein(concat(lower(pa.huisletter) , lower(pa.huisnummertoevoeging)), lower(e.toevoeging)) limit 1
	) b
) p
where pid is not null;

/* It can happen now that the same ean_code can match many adresses.
 * even though the ean_code should only be counted once.
 * */
select count(*) from ean.ean_levenshtein_matched;
select count(*) from ean.not_yet_matched nym;

select count(*) from ean.not_yet_matched nym;
select count(distinct ea_ncode) from ean.not_yet_matched nym;

select count(distinct ea_ncode) from ean.ean_levenshtein_matched elm;
select count(*) from ean.ean_levenshtein_matched elm;

drop table if exists ean.matched202110;
select *
into ean.matched202110
from ean.ean_levenshtein_matched
union
select * from ean.matched_direct md;

select count(*) from  ean.found_ean_codes;

select count(*) from ean.matched_direct;
select count(*) from ean.matched202110;

--select count(distinct ea_ncode) from ean.found_ean_codes;

select count(*) from (
select distinct ea_ncode from ean.found_ean_codes fec
) u

select count(*) from (
	select distinct ea_ncode from ean.matched202110
) u

create index on ean.matched202110 (ea_ncode);

select * from (
select *, row_number () over (partition by ea_ncode order by pid) as r from ean.matched202110 m
) bla where bla.r > 1;

select * from (
select *, row_number () over (partition by ea_ncode order by pid) as r from ean.ean_levenshtein_matched
) bla where bla.r > 1;

create index on ean.matched_direct (ea_ncode);
select count(*) from (
select *, row_number () over (partition by ea_ncode order by pid) as r from ean.matched_direct md
) bla where bla.r > 1;

select count(*) from (
select *, row_number () over (partition by ea_ncode order by pid) as r from ean.match_toevoeging mt
) bla where bla.r > 1;

select count(*) from (
select *, row_number () over (partition by ea_ncode order by pid) as r from ean.match_huisletter mh
) bla where bla.r > 1;

/*
 * select only unique eancodes from ean.matched202110.
 */

select
	*
into ean.matched202110_unique
from (
  select *, row_number () over (partition by ea_ncode order by pid) as r from ean.matched202110 m
) o where o.r = 1

create index on ean.matched202110_unique (numid);

/*
 * Find numid with multiple EAN codes. ~80.000
 *
 */
drop table if exists ean.numid_multiple_eans;
select *
into ean.numid_multiple_eans
from (
select
	*
	, row_number() over (partition by numid order by ea_ncode) as c
from ean.matched202110_unique mu
) counts where c > 1

drop table if exists ean.unique_numid_single_ean;
select
	*,
	1 as eancount
into ean.unique_numid_single_ean
from (
select
	*
	, row_number() over (partition by numid order by ea_ncode) as c
from ean.matched202110_unique mu
) counts where c = 1

create index on ean.unique_numid_single_ean (numid)

-- create index on ean.matched202110_unique (pid);
-- create index on ean.matched202110_unique (postcode);

drop table if exists ean.numid_ean_counts;
/* find number of ean codes for each numid with more then 1 ean code. */
select numid, max(c) unique_eans
into ean.numid_ean_counts
from ean.numid_multiple_eans
group by numid


select * from ean.unique_numid_single_ean;

/*
 * Update ean counts to numids.
 *
 * now the dataset is ready to be used for a lambda db export.
 */

update ean.unique_numid_single_ean nc
set eancount = unique_eans
from ean.numid_ean_counts nm
where nm.numid = nc.numid

create index on ean.numid_ean_counts (numid);
select * from ean.unique_numid_single_ean where eancount > 1;

/*
 * Create dataset grouping ean_codes to pand
 * to be used by vector tiles
 *
 * */

select pid, count(*) ean_codes
into ean.pand_eancodes
from ean.matched202110_unique group by pid;

create index on ean.pand_eancodes (pid);


select count(*) from public.eancodes202209;
select count(distinct(e)) from public.eancodes202209 e;

select numid, count(*)
into ean.numid_ean_counts_202209
from public.eancodes202209 e
where ea_ncode != ''
group by numid

create index on ean.numid_ean_counts_202209 (numid);
